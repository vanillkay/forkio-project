const gulp = require('gulp'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	autoPrefixer = require('gulp-autoprefixer'),
	clean = require('gulp-clean'),
	imageMin = require('gulp-imagemin'),
	size = require('gulp-size'),
	cache = require('gulp-cache'),
	del = require('del'),
	uglify = require('gulp-uglify'),
	cleanCSS = require('gulp-clean-css'),
	purgecss = require('gulp-purgecss'),
	minifyCSS = require('gulp-minify-css'),
	browserSync = require('browser-sync').create();

gulp.task('moveMainCss', function() {
	return (gulp
			.src([ 'src/css/reset.css', 'src/scss/styles/default/main-styles.scss' ])
			.pipe(sass().on('error', sass.logError))
			.pipe(concat('styles.min.css'))
			.pipe(purgecss({ content: [ './index.html' ] }))
			.pipe(autoPrefixer([ 'last 15 versions', '> 1%', 'ie 8', 'ie 7' ], { cascade: true }))
			// .pipe(minifyCSS())
			.pipe(gulp.dest('dist/css')) );
});

gulp.task('moveMdCss', function() {
	return (gulp
			.src('src/scss/styles/medium/screen-medium.scss')
			.pipe(sass().on('error', sass.logError))
			.pipe(concat('styles-md.css'))
			.pipe(purgecss({ content: [ './index.html' ] }))
			.pipe(autoPrefixer([ 'last 15 versions', '> 1%', 'ie 8', 'ie 7' ], { cascade: true }))
			// .pipe(minifyCSS())
			.pipe(gulp.dest('dist/css')) );
});

gulp.task('moveLgCss', function() {
	return (gulp
			.src('src/scss/styles/large/screen-large.scss')
			.pipe(sass().on('error', sass.logError))
			.pipe(concat('styles-lg.css'))
			.pipe(purgecss({ content: [ './index.html' ] }))
			.pipe(autoPrefixer([ 'last 15 versions', '> 1%', 'ie 8', 'ie 7' ], { cascade: true }))
			// .pipe(minifyCSS())
			.pipe(gulp.dest('dist/css')) );
});

gulp.task('watchCSS', function() {
	return gulp.watch('src/scss/**/*.scss', gulp.series('moveMainCss', 'moveMdCss', 'moveLgCss'));
});

gulp.task('moveCSS', gulp.series('moveMainCss', 'moveMdCss', 'moveLgCss'));

gulp.task('imagesMin', function() {
	return gulp
		.src('src/assets/**/*.png')
		.pipe(
			cache(
				imageMin({
					optimizationLevel: 5,
					progressive: true,
					interlaced: true
				})
			)
		)
		.pipe(size({ title: 'size of images' }))
		.pipe(gulp.dest('dist/images'));
});

gulp.task('jsMin', function() {
	return gulp
		.src('src/js/**/*.js')
		.pipe(concat('scripts.min.js'))
		.pipe(uglify())
		.pipe(size({ title: 'size of custom js' }))
		.pipe(gulp.dest('dist/js'));
});

gulp.task('clean-dist', function(done) {
	del.sync('dist');
	done();
});

gulp.task('build', gulp.series('clean-dist', 'moveCSS', 'imagesMin', 'jsMin'));

gulp.task('sass', function(done) {
	gulp.src('scr/scss/*.scss').pipe(sass()).pipe(gulp.dest('scr/css')).pipe(browserSync.stream());

	done();
});

gulp.task('serve', function(done) {
	browserSync.init({
		server: '../forkio-project/'
	});
	gulp
		.watch([ './src/scss/**/*.scss', './src/js/**/*.js', './index.html' ], gulp.series('moveCSS', 'jsMin'))
		.on('change', () => {
			browserSync.reload();
			done();
		});
	// gulp.watch("./index.html").on('change', () => {
	//     browserSync.reload();
	//     done();
	// });
	done();
});

gulp.task('dev', gulp.series('moveCSS', 'serve'));
