# Forkio Project 

Список использованных технологий: HTML5, CSS3, JavaScript, JQuery, Gulp.
Состав участников проекта: Шкуренко Андрей, Туляков Вячеслав.  
Задачи, которые выполняли участники: 
    Шкуренко Андрей – Сверстал блок Revolutionary Editor, секцию Here is what you get, секцию Fork Subscription Pricing. 
    Вячеслав Туляков – Сверстал шапку сайта с верхним меню (включая выпадающее меню при малом разрешении экрана), секцию People Are Talking About Fork.

    GitHub Pages link: https://stuliakov.github.io/step-project-forkio.github.io/